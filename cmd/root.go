package cmd

// Copyright © 2024 Christian Kniep <christian@qnib.org>

import (
	"os"

	"github.com/spf13/cobra"
	qGonja "gitlab.com/qnib-golang/gonja-cli/pkg/gonja"
)

// rootCmd represents the base command when called without any subcommands
var (
	fileargs []string
	varargs  []string
	rootCmd  = &cobra.Command{
		Use:   "gonja-cli <template> <dst>",
		Short: "Command-line tool to render gonja templates",
		Long:  `Command-line tool to render gonja templates`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) < 2 {
				cmd.Help()
				os.Exit(1)
			}
			qg, err := qGonja.NewQGonja(fileargs, varargs, cmd)
			if err != nil {
				panic(err)
			}
			qg.Cmd(cmd, args)
		},
	}
)

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	rootCmd.Flags().StringSliceVarP(&fileargs, "filearg", "", []string{}, "")
	rootCmd.Flags().StringSliceVarP(&varargs, "var", "", []string{}, "")
	rootCmd.Flags().String("json", "", "json file to pass to the template")
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	rootCmd.Args = cobra.MinimumNArgs(2)

}
