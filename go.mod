module gitlab.com/qnib-golang/gonja-cli

go 1.19

require (
	github.com/noirbizarre/gonja v0.0.0-20200629003239-4d051fd0be61
	github.com/spf13/cobra v1.6.1
	github.com/stretchr/testify v1.2.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/goph/emperror v0.17.1 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sirupsen/logrus v1.3.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/sys v0.2.0 // indirect
)

replace github.com/noirbizarre/gonja => github.com/ChristianKniep/gonja v0.0.0-20240413151103-d63ee9db2590
