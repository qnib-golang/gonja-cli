# Gonja CLI

Simple CLI for [noirbizarre/gonja](https://github.com/noirbizarre/gonja)


### Context

The CLI uses the environment variables of the shell as context.

In addition one can assign the content of a file to a var.

```bash
$ go run main.go --filearg NAME=./misc/files/name misc/tpl/test.gonja - 
Hello file!

Your name has `4` characters.
```

## Example

### Write to STDOUT

```bash
$ NAME=qnib go run main.go misc/tpl/test.gonja -   
Hello qnib!

Your name has `4` characters.
```

### Write to File

```bash
$ NAME=qnib  go run main.go misc/tpl/test.gonja test.txt
$ cat test.txt
Hello qnib!

Your name has `4` characters.
```
