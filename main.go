/*
Copyright © 2022 Christian Kniep <christian@qnib.org>
*/
package main

import "gitlab.com/qnib-golang/gonja-cli/cmd"

func main() {
	cmd.Execute()
}
