package utils

import "github.com/spf13/cobra"

func NewCommand() (cmd *cobra.Command) {
	cmd = &cobra.Command{
		Use:   "test-cmd",
		Short: "Dummy cmd to use for testing",
		RunE: func(cmd *cobra.Command, args []string) error {
			return nil
		},
	}
	return cmd
}
