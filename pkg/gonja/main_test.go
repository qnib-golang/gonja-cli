package qGonja

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/qnib-golang/gonja-cli/pkg/utils"
)

func TestNewQGonja(t *testing.T) {
	cmd := utils.NewCommand()
	cmd.Flags().StringP("json", "", "./data/test.json", "This is a very important input.")
	q, err := NewQGonja([]string{}, []string{"bar=baz"}, cmd)
	if err != nil {
		t.Fatal(err)
	}
	assert.Len(t, q.Varargs, 1)
}
