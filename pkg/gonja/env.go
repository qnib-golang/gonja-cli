package qGonja

import (
	"github.com/noirbizarre/gonja"
	"github.com/noirbizarre/gonja/ext/django"
	"github.com/noirbizarre/gonja/loaders"
)

func (q *QGonja) NewEnv() *gonja.Environment {
	cfg := gonja.NewConfig()
	cfg.KeepTrailingNewline = true
	loader := loaders.MustNewFileSystemLoader("./")
	env := gonja.NewEnvironment(cfg, loader)
	env.Autoescape = true
	env.Filters.Update(django.Filters)

	return env
}
