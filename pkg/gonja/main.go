package qGonja

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/noirbizarre/gonja"
	"github.com/spf13/cobra"
)

type QGonja struct {
	Fileargs []string
	Varargs  []string
	Context  map[string]interface{}
}

func NewQGonjaPlain() (qg *QGonja) {
	return &QGonja{
		Fileargs: []string{},
		Varargs:  []string{},
		Context:  make(map[string]interface{}),
	}
}

func NewQGonja(fargs, vargs []string, cmd *cobra.Command) (qg *QGonja, err error) {
	qg = NewQGonjaPlain()
	qg.Varargs = vargs
	qg.Fileargs = fargs
	ctx := gonja.Context{}
	for _, element := range os.Environ() {
		variable := strings.Split(element, "=")
		ctx[variable[0]] = variable[1]
	}
	for _, element := range qg.Fileargs {
		variable := strings.Split(element, "=")
		if len(variable) != 2 {
			fmt.Printf("Invalid filearg '%s', expect $var=file. The content of the file will be assigned to the var", element)
			os.Exit(1)
		}
		file, err := os.ReadFile(variable[1])
		if err != nil {
			panic(err)
		}
		ctx[variable[0]] = strings.TrimSpace(string(file))
	}
	for _, element := range qg.Varargs {
		variable := strings.Split(element, "=")
		if len(variable) != 2 {
			fmt.Printf("Invalid var '%s', expect $var=value", element)
			os.Exit(1)
		}
		ctx[variable[0]] = variable[1]
	}

	jsonFile, err := cmd.Flags().GetString("json")
	if err != nil {
		panic(err)
	}
	if jsonFile != "" {
		file, err := os.ReadFile(jsonFile)
		if err != nil {
			panic(err)
		}
		jsonMap := make(map[string]interface{})
		if err = json.Unmarshal(file, &jsonMap); err != nil {
			panic(err)
		}
		for k, v := range jsonMap {
			ctx[k] = v
		}
	}
	qg.Context = ctx
	return qg, nil
}
