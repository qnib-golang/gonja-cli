package qGonja

import (
	"os"

	"github.com/spf13/cobra"
)

func (qg *QGonja) Cmd(cmd *cobra.Command, args []string) {
	env := qg.NewEnv()
	tpl, err := env.FromFile(args[0])
	if err != nil {
		panic(err)
	}
	// Now you can render the template with the given
	// gonja.Context how often you want to.

	out, err := tpl.Execute(qg.Context)
	if err != nil {
		panic(err)
	}
	dst := args[1]
	if dst == "-" {
		os.Stdout.WriteString(out)
	} else {
		f, err := os.Create(dst)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		f.WriteString(out + "\n")
	}
}
