# syntax=docker/dockerfile:1.4
FROM golang:1.22-bookworm AS go
WORKDIR /go/gonja-cli
COPY . .
RUN --mount=type=cache,target=/go/pkg/mod go mod tidy
ENV CGO_ENABLED=1 GOOS=linux
RUN --mount=type=cache,target=/go/pkg/mod go build -a -ldflags '-extldflags "-static"' .

FROM alpine:3.19
RUN apk add gcompat
RUN adduser -D user
RUN apk add --no-cache ca-certificates bash
COPY --from=go /go/gonja-cli /usr/local/bin/
USER user
